// Repetition Control Structures

/*
	3 Types of looping constructs
	 1. While Loop
	 2. Do-while Loop
	 3. For Loop
*/

/*
	While loop
	- Syntax
		while(expression/condition){
			statement/s;
		}
*/

let count = 5;

while(count !== 0){
	console.log("While: " + count);
	count--; //count = count - 1, count =-1;
}

/*
Expected output:
	While 5
	While 4
	While 3
	While 2
	While 1
*/

/*
	Expected output:
		While 1
		While 2
		While 3
		While 4
		While 5
*/

let countNew = 1;

while(countNew <= 5){
	console.log("While: " + countNew);
	countNew++;
}

/*
	let count = 0;

	while(count < 5){
		++count;
		console.log(count);
	}
*/

/*
	Do While
		Syntax:
		  do{
			statement;
		  }while(expression/condition)
*/

let num1 = 1;

do{
	console.log("Do-while: " + num1);
	num1++;
}while(num1 < 6)

console.log("Number in prompt:")

let num2 = Number(prompt("Give me a number"));

do{
	console.log("Do While: " + num2);

	num2 += 1;
}while(num2 < 10)

/*
	For Loop
	Syntax:
		for(initialization; expression/condition; finalExpression){
			statement;
		}
*/

console.log("For Loop")

for(let num3 = 1; num3 <= 5; num3++){
	console.log(num3);
}

console.log("For loop with prompt");

let newNum = Number(prompt("Enter a number"));

for(let num4 = newNum; num4 < 10; num4++){
	console.log(num4)
}

console.log("Displays series of numbers based on input:")

let count2 = Number(prompt("Enter a number to count:"));

for(let num5 = 1; num5 <= count2; num5++){
	console.log(num5);
}



console.log("mini-activity");

let numa1 = Number(prompt("Give me a number."));
let numa2 = Number(prompt("Where should i end?"))

for(let numa2 = 1; numa1 <= numa2; numa1++ ){
		console.log(numa1);
	}

console.log("mini-activity");

let countNumber = Number(prompt("Hanggang saan?"));

for (let init = 1; init <= countNumber; init++) {
     console.log(init);
 } 




console.log("string iteration")
 let myString = "Juan"

 console.log(myString.length);

 console.log(myString[0]);
 console.log(myString[2]);
 console.log(myString[3]);

console.log("displaying individual letters in a string")
for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}


console.log("convert to uppercase all vowels");

let myName = "AloNzO";

for(let i = 0; i < myName.length; i++){
	// if the character of the name is a vowel, instead of dispalying the character
	// display number 3
	if(
		myName[i].toLowerCase() == 'a'  ||
		myName[i].toLowerCase() == 'e'  ||
		myName[i].toLowerCase() == 'i'  ||
		myName[i].toLowerCase() == 'o'  ||
		myName[i].toLowerCase() == 'u'
		) {
			console.log(3);
	} else{
		console.log([i]);
	}
}


console.log("continue and break")
for(let count = 0; count <= 20; count++){
	if(count % 2 == 0){
		// any number divided by 2 with 0 remainder
		continue;
	}
	console.log("continue and break: " + count)

	if(count > 10){
		break;
	}
}


let

